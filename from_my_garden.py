#!/usr/bin/python -u
import sys
import os
import json
from flask import Flask, request, Response, jsonify
import pyodbc
import logging

app = Flask(__name__)
dbCursor = None
    
@app.route("/healthcheck", methods=['GET'])
def healthcheck():
    return ('healthcheck OK', 200)
        
@app.route("/login", methods=['POST'])
def login():
    """
    {
      "username": "ivan",
      "password": "123456"
    }
    """
    try:
        data = request.get_json()
        username = data['username']
        password = data['password']
    except KeyError as e:
        return ("Missing field in payload.{}".format(e), 404)
    except Exception as e:
        return ('Request is not in JSON format', 404)
    
    row = dbCursor.execute("""
        SELECT password FROM seller
        WHERE username = ?
        """, username).fetchone()
    if row is None:
        return ("User {} doesn't exsit".format(username), 404)
    elif row.password != password:
        return ("Incorrect password", 401)
    else:
        return ("Success", 204)
    
@app.route("/signup", methods=['POST'])
def signup():
    """
    {
      "username": "ivan",
      "password": "123456",
      "phone": "021384857",
      "type": "individual",
      "location": {
                    "lat": -12,
                    "lng": 34
                  }  
    }
    """
    try:
        data = request.get_json()
        username = data['username']
        password = data['password']
        phone = data.get('phone')
        user_type = data.get('type')
        lat = data.get('location').get('lat')
        lng = data.get('location').get('lng')
    except KeyError as e:
        return ("Missing field in payload.{}".format(e), 404)
    except Exception as e:
        return ('Request is not in JSON format', 404)
    
    sqlStr = """
        INSERT INTO seller (username,
                            password,
                            phone,
                            type,
                            location)
            VALUES (?, ?, ?, ?, point(?, ?))
        """
    params = []
    params.append(username)
    params.append(password)
    params.append(phone)
    params.append(user_type)
    params.append(lng)
    params.append(lat)
    
    dbCursor.execute(sqlStr, params)

    if dbCursor.rowcount == 1:
        return ('success', 204)
    else:
        return ('User {} already exists'.format(username), 404)
    
def check_db_tables():
    if not dbCursor.tables(table='seller').fetchone():
        print("creating user table in DB")
        dbCursor.execute("""
            CREATE TABLE seller (
                username TEXT NOT NULL UNIQUE,
                password TEXT NOT NULL,
                phone TEXT,
                type TEXT,
                location POINT,
                PRIMARY KEY(username)
            )""")
        
def db_connect(host, name, user, password):
    conStr = "DRIVER={{PostgreSQL Unicode}};SERVER={0};DATABASE={1};UID={2};PWD={3}".format(host,
                                                                                            name,
                                                                                            user,
                                                                                            password)
    dbConn = pyodbc.connect(conStr, autocommit=True)
    global dbCursor
    dbCursor = dbConn.cursor()
    logging.debug("Created DB cursor {}".format(dbCursor))
    
if __name__ == "__main__":
    dbHost     = os.environ['DB_HOST']
    dbName     = os.environ['DB_NAME']
    dbUser     = os.environ['DB_USER']
    dbPass     = os.environ['DB_PASS']
    db_connect(dbHost, dbName, dbUser, dbPass)
    check_db_tables()
    app.run(host="0.0.0.0", debug=True)