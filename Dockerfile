FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt .
COPY . .

RUN apt-get update \ 
	&& apt-get -y install libodbc1 unixodbc-dev odbc-postgresql
	
RUN pip install -r requirements.txt
	

CMD [ "python", "-u", "./from_my_garden.py" ]
